FROM scratch
MAINTAINER "Denis Silakov" <dsilakov@gmail.com>
ADD rootfs-systemd.tar.xz /
ENTRYPOINT /bin/sh